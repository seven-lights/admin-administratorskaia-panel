<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

YiiAsset::register($this);

$this->title = $model->nick . ' - Системный раздел';
$this->params['breadcrumbs'][] = ['label' => 'Системный раздел', 'url' => [ 'system/site/index']];
$this->params['breadcrumbs'][] = $model->nick;
?>
<div class="user-view">
    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nick',
	        'permission',
	        [
		        'attribute' => 'created_at',
		        'format' => ['date', 'php:' . Yii::$app->params['dateFormat'] ],
	        ],
	        [
		        'attribute' => 'updated_at',
		        'format' => ['date', 'php:' . Yii::$app->params['dateFormat'] ],
	        ],
        ],
    ]) ?>
</div>
