<?php
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

YiiAsset::register($this);

$this->title = $model->name . ' - Системный раздел';
$this->params['breadcrumbs'][] = ['label' => 'Системный раздел', 'url' => [ 'system/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;

$route = json_decode($model->route);
$model->route = $route[0];
unset($route[0]);
if($route) {
	$model->route .= http_build_query($route);
}
?>
<div class="menu-view">
    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'icon',
            'route',
            'number',
            'permission',
            'service.name',
            'parent.name',
	        [
		        'attribute' => 'created_at',
		        'format' => ['date', 'php:' . Yii::$app->params['dateFormat'] ],
	        ],
	        [
		        'attribute' => 'updated_at',
		        'format' => ['date', 'php:' . Yii::$app->params['dateFormat'] ],
	        ],
        ],
    ]) ?>

</div>
