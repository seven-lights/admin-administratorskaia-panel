<?php

use app\models\Menu;
use app\models\Service;
use app\extensions\Permissions;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */

$services = [];
$models = Service::find()->all();
foreach ($models as $val) {
	/* @var $val Service */
	$services[ $val->id ] = $val->name;
}

$parents = Menu::getParents($model->service_id, $model->isNewRecord);
$numbers = Menu::getNumbers($model->service_id, $model->parent_id, $model->isNewRecord);
if($model->route) {
	$route = json_decode($model->route);
	$model->route = $route[0];
	unset($route[0]);
	if ($route) {
		$model->route .= http_build_query($route);
	}
}
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'icon')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'route')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'number')->dropDownList($numbers) ?>

    <?= $form->field($model, 'service_id')->dropDownList($services) ?>

    <?= $form->field($model, 'parent_id')->dropDownList($parents) ?>

	<?php
	if($model->isNewRecord) {
		$this->registerJs('
			(function($) {
				function loadNumbers() {
					$("#menu-number").empty()
						.html(\'<option value="">Загрузка...</option>\');

					$.ajax({
						type: "GET",
						url: "' . Yii::$app->urlManager->createUrl(['system/menu/get-numbers']) . '",
						data: "service_id=" + $("#menu-service_id").val()
						    + "&parent_id=" + $("#menu-parent_id").val()
						    + "&is_new_record=" + ' . (int)$model->isNewRecord . ',
						success: function(data){
							var html = "";
							for (var key in data) {
								html += \'<option value="\' + key + \'">\' + data[key] + \'</option>\';
							}
							$("#menu-number").html(html);
						}
					});
				}
				function loadParents() {
					$("#menu-parent_id").empty()
						.html(\'<option value="">Загрузка...</option>\');

					$.ajax({
						type: "GET",
						url: "' . Yii::$app->urlManager->createUrl(['system/menu/get-parents']) . '",
						data: "service_id=" + $("#menu-service_id").val()
						    + "&is_new_record=" + ' . (int)$model->isNewRecord . ',
						success: function(data){
							var html = "";
							for (var key in data) {
								html += \'<option value="\' + key + \'">\' + data[key] + \'</option>\';
							}
							$("#menu-parent_id").html(html);
						}
					});
				}
				$("#menu-service_id").change(function() {
					loadNumbers();
					loadParents();
				});
				$("#menu-parent_id").change(function() {
					loadNumbers();
				});
				if(' . (int)$model->isNewRecord . ') {
					loadNumbers();
					loadParents();
				}
			})(jQuery);
		');
	}
	?>

	<?= $form->field($model, 'permission')->dropDownList(Permissions::$permissions, ['prompt' => 'Определить автоматически']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
