<?php
use general\widgets\api\LoginForm;

$this->title = 'Аутентификация';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= LoginForm::widget([
	'service' => Yii::$app->id,
	'view' => 'login',
	'retUrl' => '//' . Yii::$app->params['api']['loginUrls'][ Yii::$app->id ],
	'class' => 'login',
	'id' => 'login',
]);
?>