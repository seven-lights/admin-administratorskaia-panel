<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\ServiceChangeForm */
/* @var $data array */
$this->registerCssFile('/css/change_service.css');
$this->title = 'Выбор сервиса';

$drop_down_data = [];
$many_sites = [];
foreach ($data as $val) {
	$drop_down_data[ $val['nick'] ] = $val['name'];
	$many_sites[ $val['nick'] ] = $val['many_sites'];
}
?>

<div id="login">

    <?php $form = ActiveForm::begin([
	    'id' => 'loginForm',
	    'fieldConfig' => [
		    'template' => '<div class="field">{input}</div>',
	    ],
    ]); ?>

	<div id="logo">
		<img src="/i/logo.jpg" alt="Семь огней - Seven lights" title="Семь огней - Seven lights">
	</div>

	<?= $form->errorSummary($model, ['header' => '']) ?>

    <?= $form->field($model, 'service')->dropDownList($drop_down_data) ?>

    <?= $form->field($model, 'site', [
	    'options' => [
		    'style' => 'display: none'
	    ],
    ])->dropDownList([]) ?>
	<?php
	//@todo сделать кэширование на клиенте
	$this->registerJs('
	(function($) {
		var many_sites = ' . json_encode($many_sites) . ';

		$("#servicechangeform-service").change(function() {
			if(many_sites[ $(this).val() ] == 1) {
				$(".field-servicechangeform-site").show();

				$("#servicechangeform-site").empty();
				$("#servicechangeform-site").html(\'<option value="">Загрузка...</option>\');

				$.ajax({
					type: "GET",
					url: "' . Yii::$app->urlManager->createUrl(['/service/get-sites']) . '",
					data: "service=" + $(this).val(),
					success: function(data){
						var html = "";
						for (var key in data) {
							html += \'<option value="\' + key + \'">\' + data[key] + \'</option>\';
						}
						$("#servicechangeform-site").html(html);
					}
				});
			} else {
				$(".field-servicechangeform-site").hide();
			}
		});
		$("#servicechangeform-service").change();
	})(jQuery)
	');
	?>

    <div class="submit">
        <?= Html::submitButton('Выбрать') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
