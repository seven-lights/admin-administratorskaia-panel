<?php
/**
 * Project: Auth - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
/** @var string $result */
/** @var string $content */
echo json_encode(
	array_merge(
		['result' => $result],
		$content
	)
);