<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\CategoryOfArticleForm */

$this->title = 'Добавить раздел';
$this->params['breadcrumbs'][] = ['label' => 'Моё сбалансированное питание', 'url' => [ 'diet/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы статей', 'url' => [ 'diet/category-of-article/index']];
$this->params['breadcrumbs'][] = 'Добавить раздел';
?>
<div class="post-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
