<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\CategoryOfArticleForm */

$this->title = 'Изменить раздел: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Моё сбалансированное питание', 'url' => [ 'diet/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Разделы статей', 'url' => [ 'diet/category-of-article/index']];
$this->params['breadcrumbs'][] = 'Изменить раздел';
?>
<div class="post-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
