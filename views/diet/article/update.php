<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\PostForm */

$this->title = 'Изменить запись: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Моё сбалансированное питание', 'url' => [ 'diet/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Все статьи', 'url' => [ 'diet/article/index']];
$this->params['breadcrumbs'][] = 'Изменить запись';
?>
<div class="post-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
