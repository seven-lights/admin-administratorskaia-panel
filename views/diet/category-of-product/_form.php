<?php

use general\ext\api\diet\DietApi;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\CategoryOfArticleForm */
/* @var $form yii\widgets\ActiveForm */

/**
 * @param $cats
 * @param $categories
 * @param string $prefix
 * @return array
 */
function genMenu($cats, &$categories, $prefix = '') {
	$parents = [];
	foreach ($cats as $cat) {
		$parents[ $cat['id'] ] = $prefix . $cat['name'];

		if(isset($categories[ $cat['id'] ])) {
			$parents = $parents + genMenu($categories[ $cat['id'] ], $categories, $prefix . '-');
		}
	}
	return $parents;
}

$parents = ['(не выбрано)'];
$res = DietApi::categoryOfProductIndex();
if($res['result'] == 'success') {
	$categories = [];
	foreach ($res['categories'] as $cat) {
		if($cat['id'] == $model->id) {
			continue;
		}

		if(is_null($cat['parent_id'])) {
			$categories[0][] = $cat;
		} else {
			$categories[ $cat['parent_id'] ][] = $cat;
		}
	}

	if(isset($categories[0])) {
		$parents = $parents + genMenu($categories[0], $categories);
	}
}

?>

<div class="form">
    <?php $form = ActiveForm::begin(); ?>

	<?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'number')->dropDownList([]) ?>

    <?= $form->field($model, 'parent_id')->dropDownList($parents) ?>

	<?php
	$this->registerJs('
		(function($) {
			function loadNumber() {
				$("#categoryofproductform-number").html(\'<option value="">Загрузка..</option>\');
				$.ajax({
					type: "GET",
					url: "' . Yii::$app->urlManager->createUrl(['diet/category-of-product/max-number']) . '",
					data: "parent_id=" + $("#categoryofproductform-parent_id").val()
					    + "&is_new_record=" + ' . (int)$model->isNewRecord . ',
					success: function(data){
						var html = "";
						var max = parseInt(data["max"]);
						max = max > 0 ? max : 1;
						for(var i = 1; i <= max + ' . (int)$model->isNewRecord . '; i++) {
							html += \'<option value="\' + i + \'">\' + i + \'</option>\';
						}
						$("#categoryofproductform-number").html(html);
					}
				});
			}
			$("#categoryofproductform-parent_id").change(function() {
				loadNumber();
			});
			loadNumber();
		})(jQuery);
	');
	?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить') ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
