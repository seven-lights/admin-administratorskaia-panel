<?php

use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = ['label' => 'Моё сбалансированное питание', 'url' => ['diet/site/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
	        [
		        'header' => $searchModel->getAttributeLabel('id'),
		        'attribute' => 'id',
	        ],
	        [
		        'header' => $searchModel->getAttributeLabel('name'),
		        'attribute' => 'name',
	        ],
	        [
		        'header' => $searchModel->getAttributeLabel('category.name'),
		        'attribute' => 'category.name',
	        ],
            [
	            'class' => 'yii\grid\ActionColumn',
	            'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
