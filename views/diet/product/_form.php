<?php

use general\ext\api\diet\DietApi;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\ProductForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin([
	    'options' => [
		    'enctype'=>'multipart/form-data',
	    ],
    ]); ?>

	<?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

	<?= $form->field($model, 'description')->widget(Widget::className(), [
		'settings' => [
			'lang' => 'ru',
			'minHeight' => 200,
		]
	]); ?>

	<? if(!$model->isNewRecord && $model->image_links !== []) { ?>
		<?= Html::beginTag('div') ?>
		<?= Html::img($model->image_links['small']) ?>
		<?= Html::endTag('div') ?>
	<? } ?>

	<?= $form->field($model, 'image')->fileInput() ?>

    <?= $form->field($model, 'protein')->textInput() ?>

    <?= $form->field($model, 'fat')->textInput() ?>

    <?= $form->field($model, 'carbohydrate')->textInput() ?>

    <?= $form->field($model, 'calorie')->textInput() ?>

    <?= $form->field($model, 'dietary_fiber')->textInput() ?>

    <?= $form->field($model, 'cholesterol')->textInput() ?>

    <?= $form->field($model, 'water')->textInput() ?>

    <?= $form->field($model, 'ca')->textInput() ?>

    <?= $form->field($model, 'fe')->textInput() ?>

    <?= $form->field($model, 'na')->textInput() ?>

    <?= $form->field($model, 'k')->textInput() ?>

    <?= $form->field($model, 'mg')->textInput() ?>

    <?= $form->field($model, 'p')->textInput() ?>

    <?= $form->field($model, 'co')->textInput() ?>

    <?= $form->field($model, 'cr')->textInput() ?>

    <?= $form->field($model, 'cu')->textInput() ?>

    <?= $form->field($model, 'mn')->textInput() ?>

    <?= $form->field($model, 's')->textInput() ?>

    <?= $form->field($model, 'si')->textInput() ?>

    <?= $form->field($model, 'v')->textInput() ?>

    <?= $form->field($model, 'zn')->textInput() ?>

    <?= $form->field($model, 'mo')->textInput() ?>

    <?= $form->field($model, 'f')->textInput() ?>

    <?= $form->field($model, 'i')->textInput() ?>

    <?= $form->field($model, 'se')->textInput() ?>

    <?= $form->field($model, 'b')->textInput() ?>

    <?= $form->field($model, 'cl')->textInput() ?>

    <?= $form->field($model, 'a')->textInput() ?>

    <?= $form->field($model, 'b_car')->textInput() ?>

    <?= $form->field($model, 'b1')->textInput() ?>

    <?= $form->field($model, 'b2')->textInput() ?>

    <?= $form->field($model, 'b4')->textInput() ?>

    <?= $form->field($model, 'b5')->textInput() ?>

    <?= $form->field($model, 'b6')->textInput() ?>

    <?= $form->field($model, 'b7')->textInput() ?>

    <?= $form->field($model, 'b9')->textInput() ?>

    <?= $form->field($model, 'b12')->textInput() ?>

    <?= $form->field($model, 'c')->textInput() ?>

    <?= $form->field($model, 'd')->textInput() ?>

    <?= $form->field($model, 'e')->textInput() ?>

    <?= $form->field($model, 'pp')->textInput() ?>

    <?= $form->field($model, 'k_phylloquinone')->textInput() ?>

    <?= $form->field($model, 'tryptophan')->textInput() ?>

    <?= $form->field($model, 'lysine')->textInput() ?>

    <?= $form->field($model, 'methionine')->textInput() ?>

    <?= $form->field($model, 'threonine')->textInput() ?>

    <?= $form->field($model, 'leucine')->textInput() ?>

    <?= $form->field($model, 'isoleucine')->textInput() ?>

    <?= $form->field($model, 'phenylalanine')->textInput() ?>

    <?= $form->field($model, 'valine')->textInput() ?>

    <?= $form->field($model, 'owner_id')->textInput() ?>

    <?= $form->field($model, 'copy_from')->textInput() ?>

	<?
	/**
	 * @param $cats
	 * @param $categories
	 * @param string $prefix
	 * @return array
	 */
	function genTree($cats, &$categories, $prefix = '') {
		$parents = [];
		foreach ($cats as $cat) {
			$parents[ $cat['id'] ] = $prefix . $cat['name'];

			if(isset($categories[ $cat['id'] ])) {
				$parents = $parents + genTree($categories[ $cat['id'] ], $categories, $prefix . '-');
			}
		}
		return $parents;
	}

	$res = DietApi::categoryOfProductIndex();
	$categories = [];
	if($res['result'] == 'success') {
		foreach ($res['categories'] as $cat) {
			if(is_null($cat['parent_id'])) {
				$categories[0][] = $cat;
			} else {
				$categories[ $cat['parent_id'] ][] = $cat;
			}
		}

		if(isset($categories[0])) {
			$categories = genTree($categories[0], $categories);
		}
	}
	?>

    <?= $form->field($model, 'category_id')->dropDownList(['0' => '(не выбрано)'] + $categories) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
