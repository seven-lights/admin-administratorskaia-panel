<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use app\models\search\PageSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Управление страницами';
$this->params['breadcrumbs'][] = 'Все страницы';

/* @var yii\data\ArrayDataProvider $dataProvider */
/* @var PageSearch $searchModel */
?>

<div class="menu-index">
	<p>
		<?= Html::a('Добавить страницу', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		//'filterModel' => $searchModel,
		'layout' => '{items}{pager}',
		'columns' => [
			[
				'label' => $searchModel->getAttributeLabel('id'),
				'attribute' => 'id',
				'filterInputOptions' => ['style' => 'width: 50px;'],
			],
			[
				'label' => $searchModel->getAttributeLabel('title'),
				'attribute' => 'title',
			],
			[
				'class' => ActionColumn::className(),
				'template' => '{update} {delete}'
			],
		],
	]) ?>
</div>