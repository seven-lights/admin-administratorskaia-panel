<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\PostForm */

$this->title = 'Создать страницу';
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => [ 'page/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Все страницы', 'url' => [ 'page/page/index']];
$this->params['breadcrumbs'][] = 'Создать страницу';
?>
<div class="post-create">
    <?= $this->render('_form', [
        'model' => $model,
	    'domain' => $domain,
    ]) ?>
</div>
