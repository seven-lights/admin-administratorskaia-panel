<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\PostForm */

$this->title = 'Изменить страницу: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => [ 'page/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Все страницы', 'url' => [ 'page/page/index']];
$this->params['breadcrumbs'][] = 'Изменить страницу';
?>
<div class="post-update">
    <?= $this->render('_form', [
        'model' => $model,
	    'domain' => $domain,
    ]) ?>
</div>
