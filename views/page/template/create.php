<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\page\TemplateForm */

$this->title = 'Создать шаблон';
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => [ 'page/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Все шаблоны', 'url' => [ 'page/page/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">
    <?= $this->render('_form', [
        'model' => $model,
	    'domain' => $domain,
    ]) ?>
</div>
