<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */
use app\models\search\PageSearch;
use yii\grid\ActionColumn;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'Управление шаблонами';
$this->params['breadcrumbs'][] = 'Все шаблоны';

/* @var yii\data\ArrayDataProvider $dataProvider */
/* @var PageSearch $searchModel */
?>

<div class="menu-index">
	<p>
		<?= Html::a('Создать шаблон', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= \yii\grid\GridView::widget([
		'dataProvider' => $dataProvider,
		'layout' => '{items}{pager}',
		'columns' => [
			[
				'label' => $searchModel->getAttributeLabel('id'),
				'attribute' => 'id',
				'filterInputOptions' => ['style' => 'width: 50px;'],
			],
			[
				'label' => $searchModel->getAttributeLabel('name'),
				'attribute' => 'name',
			],
			[
				'class' => ActionColumn::className(),
				'template' => '{update} {delete}'
			],
		],
	]) ?>
</div>