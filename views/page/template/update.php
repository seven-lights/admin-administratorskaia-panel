<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\page\TemplateForm */

$this->title = 'Изменить шаблон: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => [ 'page/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Все шаблоны', 'url' => [ 'page/page/index']];
$this->params['breadcrumbs'][] = 'Изменить шаблон';
?>
<div class="post-update">
    <?= $this->render('_form', [
        'model' => $model,
	    'domain' => $domain,
    ]) ?>
</div>
