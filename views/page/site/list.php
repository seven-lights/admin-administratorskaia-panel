<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сайты';
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['page/site/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
	<p>
		<?= Html::a('Создать сайт', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
	        [
		        'header' => $searchModel->getAttributeLabel('id'),
		        'attribute' => 'id',
	        ],
	        [
		        'header' => $searchModel->getAttributeLabel('name'),
		        'attribute' => 'name',
	        ],
	        [
		        'header' => $searchModel->getAttributeLabel('domain'),
		        'attribute' => 'domain',
	        ],
	        [
		        'class' => ActionColumn::className(),
		        'template' => '{index} {update} {delete}',
		        'urlCreator' => function ($action, $model, $key, $index)
		        {
			        $params = is_array($key) ? $key : ['site' => (string) $key];
			        $params[0] = $action;

			        return Url::toRoute($params);
		        },
		        'buttons' => [
			        'index' => function ($url, $model, $key) {
				        $options = [
					        'title' => Yii::t('yii', 'View'),
					        'aria-label' => Yii::t('yii', 'View'),
					        'data-pjax' => '0',
				        ];
				        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
			        }
		        ]
	        ],
        ],
    ]); ?>

</div>
