<?php
/* @var $this yii\web\View */
/* @var $model app\models\forms\page\SiteForm */

$this->title = 'Создать сайт';
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => [ 'page/site/index']];
$this->params['breadcrumbs'][] = ['label' => 'Все сайты', 'url' => [ 'page/site/list']];
$this->params['breadcrumbs'][] = 'Создать сайт';
?>
<div class="post-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
