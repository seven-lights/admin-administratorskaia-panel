<?php
$generalParams = require(dirname(dirname(__DIR__)) . '/general/config/params.php');
$params = [
	'adminEmail' => 'admin@dudev.ru',
	'api' => require(dirname(dirname(__DIR__)) . '/general/config/api.php'),
	'durationAuth' => 3600 * 24 * 30, //хранить auth_key месяц
];

return array_merge($params, $generalParams);
