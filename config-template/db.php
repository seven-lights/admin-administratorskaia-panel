<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=admin',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
	'enableSchemaCache' => true,
	'schemaCacheDuration' => 3600,
];
