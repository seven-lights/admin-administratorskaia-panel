<?php

use yii\db\Schema;
use yii\db\Migration;

class m150223_044532_create_menu_table extends Migration
{
    public function up()
    {
	    $this->createTable('menu', [
		    'id' => Schema::TYPE_PK,
		    'name' => Schema::TYPE_STRING . '(50) NOT NULL',
		    'icon' => Schema::TYPE_STRING . ' NOT NULL DEFAULT "" COMMENT "Класс для отображения пиктограмы около понкта меню"',
		    'route' => Schema::TYPE_STRING . ' NOT NULL',
		    'number' => Schema::TYPE_SMALLINT . ' NOT NULL',
		    'permission' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'service_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'parent_id' => Schema::TYPE_INTEGER,
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);
	    $this->addForeignKey('service_id_FK_menu', 'menu', 'service_id', 'service', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('parent_id_FK_menu', 'menu', 'parent_id', 'menu', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m150208_044532_create_menu_table cannot be reverted.\n";

        return false;
    }
}