<?php

use yii\db\Schema;
use yii\db\Migration;

class m150224_091628_add_controllers_in_service_table extends Migration
{
    public function up()
    {
	    $this->addColumn('service', 'controllers', Schema::TYPE_STRING . ' NOT NULL');
    }

    public function down()
    {
        echo "m150224_091628_add_controllers_in_service_table cannot be reverted.\n";

        return false;
    }
}
