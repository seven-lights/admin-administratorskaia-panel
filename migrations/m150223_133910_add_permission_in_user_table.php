<?php

use yii\db\Schema;
use yii\db\Migration;

class m150223_133910_add_permission_in_user_table extends Migration
{
    public function up()
    {
	    $this->addColumn('user', 'permission', Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m150223_133910_add_permission_in_user_table cannot be reverted.\n";

        return false;
    }
}
