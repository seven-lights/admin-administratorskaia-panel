<?php
/**
 * Project: Admin - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions;


use app\models\Service;
use general\ext\api\BaseApi;
use yii\base\Object;

class Permissions extends Object {
	const PERMISSION_CREATE = 1;
	const PERMISSION_READ = 2;
	const PERMISSION_UPDATE = 3;
	const PERMISSION_DELETE = 4;
	const PERMISSION_FULL = 5;

	public static $permissions = [
		self::PERMISSION_CREATE => 'create',
		self::PERMISSION_READ => 'read',
		self::PERMISSION_UPDATE => 'update',
		self::PERMISSION_DELETE => 'delete',
		self::PERMISSION_FULL => 'full',
	];
	public $user_permissions = [];

	public function __construct($config = []) {
		parent::__construct($config);
	}

	public static function getPermissionFromAction($action) {
		switch($action) {
			case 'list':
			case 'view':
			case 'index':
				return self::PERMISSION_READ;
			case 'create':
				return self::PERMISSION_CREATE;
			case 'update':
				return self::PERMISSION_UPDATE;
			case 'delete':
				return self::PERMISSION_DELETE;
			default:
				return null;
		}
	}

	public static function decode($value) {
		$value = trim($value);

		if($value == 'full') {
			return 'full';
		}

		$rows = explode("\n", $value);
		$ret = [];
		foreach ($rows as $row) {
			if(!$row) {
				continue;
			}
			$data = explode(':', $row);
			if(!isset($data[1])) {
				return null;
			}
			preg_match('#([a-z]+)\(([0-9,]+|full)?\)#i', $data[0], $res);
			if(!isset($res[1])
			|| !$m_service = Service::findOne(['nick' => $res[1]])) {
				return null;
			}
			$service = $res[1];
			/* @var $m_service Service */
			//если сервис многосайтовый
			if($m_service->many_sites) {
				if(!isset($res[2])) {
					return null;
				}
				//проверяем наличие указанных сайтов на сервисе
				$class = BaseApi::getInplementationName($service);
				/* @var $class BaseApi */
				$service_sites = $class::getSites();
				$sites = explode(',', $res[2]);
				foreach ($sites as $site) {
					if(!isset($service_sites[ $site ])) {
						return null;
					}
				}

			}

			$controllers = [];
			foreach (explode(';', $data[1]) as $val) {
				if(!$val) {
					continue;
				}
				preg_match('#([a-z]+)\(([a-z,]+|full)?\)#i', $val, $res);
				if(!isset($res[1])
				|| !in_array($res[1], explode(',', $m_service->controllers), true)) {
					return null;
				}
				$controller = $res[1];
				if(!isset($res[2])) {
					return null;
				}
				$permissions_controller = explode(',', $res[2]);
				if(!self::checkPermissions($permissions_controller)) {
					return null;
				}

				$controllers[ $controller ] = $permissions_controller;
			}

			$ret[ $service ] = $controllers;
			if(isset($sites)) {
				$ret[ $service ]['_sites'] = $sites;
			}
		}
		return $ret;
	}
	private static function checkPermissions($permissions) {
		foreach ((array)$permissions as $val) {
			if(!in_array($val, self::$permissions, true)) {
				return false;
			}
		}
		return true;
	}
} 