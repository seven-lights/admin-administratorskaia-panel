<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions\api\page;


use app\models\forms\page\PageForm;
use app\models\search\PageSearch;
use general\controllers\api\Controller;
use general\ext\api\page\PageApi;
use yii\web\NotFoundHttpException;

/**
 * Class PageAdminControllerTrait
 * @package app\extensions\api
 */
trait PageAdminControllerTrait {
	public function actionIndex() {
		/* @var $this \app\controllers\page\PageController */
		if(empty(self::$domain)) {
			throw new \InvalidArgumentException();
		}

		$searchModel = new PageSearch();
		$dataProvider = $searchModel->search([
			'domain' => self::$domain,
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}
	public function actionUpdate($id) {
		/* @var $this \app\controllers\page\PageController */
		if(empty(self::$domain)) {
			throw new \InvalidArgumentException();
		}

		$model = new PageForm();
		$model->isNewRecord = false;

		$res = PageApi::pageUpdate(self::$domain, $id);
		if($res['result'] == 'error') {
			throw new NotFoundHttpException();
		}
		$model->setAttributes($res['page']);

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			if($model->validate()) {
				$res = PageApi::pageUpdate(
					self::$domain,
					$id,
					$model->getAttributes()
				);
				if($res['result'] == 'success') {
					$this->redirect(['update', 'id' => $id]);
				} else {
					$errors = [
						Controller::ERROR_UNKNOWN => 'info',
						Controller::ERROR_DB => 'info',
						Controller::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						Controller::ERROR_NO_DATA => 'info',

						Controller::ERROR_ILLEGAL_PAGE_NICK => 'nick',
						Controller::ERROR_ILLEGAL_PAGE_TITLE => 'title',
						Controller::ERROR_ILLEGAL_PAGE_KEYWORDS => 'keywords',
						Controller::ERROR_ILLEGAL_PAGE_DESCRIPTION => 'description',
						Controller::ERROR_ILLEGAL_PAGE_TEXT => 'text',
						Controller::ERROR_ILLEGAL_PAGE_TEMPLATE => 'template_id',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == Controller::ERROR_NO_SITE
						|| $error['code'] == Controller::ERROR_NO_POST) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', Controller::$_errors[ Controller::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('update', [
			'model' => $model,
			'domain' => self::$domain,
		]);
	}
	public function actionCreate() {
		/* @var $this \app\controllers\page\PageController */
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		$model = new PageForm();
		$model->site_id = self::$site;

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			if($model->validate()) {
				$res = PageApi::pageCreate(
					self::$domain,
					$model->getAttributes()
				);
				if($res['result'] == 'success') {
					$this->redirect(['index']);
				} else {
					$errors = [
						Controller::ERROR_UNKNOWN => 'info',
						Controller::ERROR_DB => 'info',
						Controller::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						Controller::ERROR_NO_DATA => 'info',

						Controller::ERROR_ILLEGAL_PAGE_NICK => 'nick',
						Controller::ERROR_ILLEGAL_PAGE_TITLE => 'title',
						Controller::ERROR_ILLEGAL_PAGE_KEYWORDS => 'keywords',
						Controller::ERROR_ILLEGAL_PAGE_DESCRIPTION => 'description',
						Controller::ERROR_ILLEGAL_PAGE_TEXT => 'text',
						Controller::ERROR_ILLEGAL_PAGE_TEMPLATE => 'template_id',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == Controller::ERROR_NO_SITE) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', Controller::$_errors[ Controller::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('create', [
			'model' => $model,
			'domain' => self::$domain,
		]);
	}
	public function actionDelete($id) {
		/* @var $this \app\controllers\page\PageController */
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		PageApi::pageDelete(self::$domain, $id);
		$this->redirect(['index']);
	}
}