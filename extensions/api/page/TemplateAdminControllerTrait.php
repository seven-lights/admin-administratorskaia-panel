<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions\api\page;


use app\models\forms\page\PageForm;
use app\models\forms\page\TemplateForm;
use app\models\search\TemplateSearch;
use general\controllers\api\Controller;
use general\ext\api\page\PageApi;
use yii\web\NotFoundHttpException;

/**
 * Class TemplateAdminControllerTrait
 * @package app\extensions\api\page
 */
trait TemplateAdminControllerTrait {
	public function actionIndex() {
		/* @var $this \app\controllers\page\PageController */
		if(empty(self::$domain)) {
			throw new \InvalidArgumentException();
		}

		$searchModel = new TemplateSearch();
		$dataProvider = $searchModel->search([
			'domain' => self::$domain,
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}
	public function actionUpdate($id) {
		/* @var $this \app\controllers\page\TemplateController */
		if(empty(self::$domain)) {
			throw new \InvalidArgumentException();
		}

		$model = new TemplateForm();
		$model->isNewRecord = false;

		$res = PageApi::templateUpdate(self::$domain, $id);
		if($res['result'] == 'error') {
			throw new NotFoundHttpException();
		}
		$model->setAttributes($res['template']);

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			if($model->validate()) {
				$res = PageApi::templateUpdate(
					self::$domain,
					$id,
					$model->getAttributes()
				);
				if($res['result'] == 'success') {
					$this->redirect(['update', 'id' => $id]);
				} else {
					$errors = [
						Controller::ERROR_UNKNOWN => 'info',
						Controller::ERROR_DB => 'info',
						Controller::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						Controller::ERROR_NO_DATA => 'info',

						Controller::ERROR_ILLEGAL_TEMPLATE_NAME => 'name',
						Controller::ERROR_ILLEGAL_TEMPLATE_HEAD => 'head',
						Controller::ERROR_ILLEGAL_TEMPLATE_BODY_START => 'body_start',
						Controller::ERROR_ILLEGAL_TEMPLATE_BODY_END => 'body_end',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == Controller::ERROR_NO_SITE
						|| $error['code'] == Controller::ERROR_NO_POST) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', Controller::$_errors[ Controller::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('update', [
			'model' => $model,
			'domain' => self::$domain,
		]);
	}
	public function actionCreate() {
		/* @var $this \app\controllers\page\TemplateController */
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		$model = new TemplateForm();
		$model->site_id = self::$site;

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			if($model->validate()) {
				$res = PageApi::templateCreate(
					self::$domain,
					$model->getAttributes()
				);
				if($res['result'] == 'success') {
					$this->redirect(['index']);
				} else {
					$errors = [
						Controller::ERROR_UNKNOWN => 'info',
						Controller::ERROR_DB => 'info',
						Controller::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						Controller::ERROR_NO_DATA => 'info',

						Controller::ERROR_ILLEGAL_TEMPLATE_NAME => 'name',
						Controller::ERROR_ILLEGAL_TEMPLATE_HEAD => 'head',
						Controller::ERROR_ILLEGAL_TEMPLATE_BODY_START => 'body_start',
						Controller::ERROR_ILLEGAL_TEMPLATE_BODY_END => 'body_end',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == Controller::ERROR_NO_SITE) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', Controller::$_errors[ Controller::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('create', [
			'model' => $model,
			'domain' => self::$domain,
		]);
	}
	public function actionDelete($id) {
		/* @var $this \app\controllers\page\PageController */
		if(empty(static::$domain)) {
			throw new \InvalidArgumentException();
		}
		PageApi::templateDelete(self::$domain, $id);
		$this->redirect(['index']);
	}
}