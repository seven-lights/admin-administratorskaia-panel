<?php
/**
 * Project: Blog Platform - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\extensions;
use yii\web\Cookie;


/**
 * Class Controller
 */
class Controller extends \yii\web\Controller {
	public $service = '';
	protected static $site;
	public function __construct($id, $module, $config = []) {
		parent::__construct($id, $module, $config);

		$site = \Yii::$app->request->get('site');
		if($site || $site === '0') {
			self::$site = \Yii::$app->request->get('site');

			\Yii::$app->response->cookies->add(
				new Cookie([
					'name' => 'site',
					'value' => self::$site,
				])
			);
		} else {
			self::$site = \Yii::$app->request->cookies->getValue('site');
		}
	}
} 