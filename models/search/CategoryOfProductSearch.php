<?php

namespace app\models\search;

use app\models\forms\diet\CategoryOfProductForm;
use general\ext\api\diet\DietApi;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class CategoryOfProductSearch extends CategoryOfProductForm {
	public $parent_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['parent_id', 'integer'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params) {
	    $this->setAttributes($params['CategoryOfArticleSearch']);

	    if (!$this->validate()) {
		    return new ArrayDataProvider();
	    }
	    $data = DietApi::categoryOfProductIndex($this->parent_id, $params['sort']);
	    if($data['result'] == 'success') {
		    $dataProvider = new ArrayDataProvider([
			    'models' => $data['categories'],
			    'key' => 'id',
			    'sort' => [
					'attributes' => ['id', 'name', 'number', 'parent_id'],
				],
			    'pagination' => false,
		    ]);

		    return $dataProvider;
	    } else {
		    return new ArrayDataProvider();
	    }
    }
}
