<?php

namespace app\models\search;

use app\models\forms\page\SiteForm;
use general\ext\api\page\PageApi;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class SiteSearch extends SiteForm {
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @return ArrayDataProvider
     */
    public function search() {
	    $data = PageApi::siteList();
	    if($data['result'] == 'success') {
		    $dataProvider = new ArrayDataProvider([
			    'models' => $data['sites'],
			    'key' => 'id',
		    ]);

		    return $dataProvider;
	    } else {
		    return new ArrayDataProvider();
	    }
    }
}
