<?php

namespace app\models\search;

use app\models\forms\diet\ProductForm;
use general\ext\api\diet\DietApi;
use Yii;
use yii\data\ArrayDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class ProductSearch extends ProductForm {
	public $owner_id;
	public $category_id;
	public $copy_from;
	public $name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'category_id', 'copy_from'], 'integer'],
	        ['name', 'string'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params) {
	    $this->setAttributes($params['ProductSearch']);

	    if (!$this->validate()) {
		    return new ArrayDataProvider();
	    }

	    $data = DietApi::productIndex(
		    $params,
		    $params['sort']
	    );
	    if($data['result'] == 'success') {
		    $dataProvider = new ArrayDataProvider([
			    'models' => $data['products'],
			    'key' => 'id',
			    'sort' => [
					'attributes' => ['name'],
				],
			    'pagination' => [
				    'totalCount' => (int)$data['totalCount'],
				    'pageSize' => 30,
			    ],
		    ]);

		    return $dataProvider;
	    } else {
		    return new ArrayDataProvider();
	    }
    }
}
