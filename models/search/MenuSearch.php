<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Menu;

/**
 * MenuSearch represents the model behind the search form about `app\models\Menu`.
 */
class MenuSearch extends Menu
{
	public function attributes() {
		// add related fields to searchable attributes
		return array_merge(parent::attributes(), ['service.name', 'parent.name']);
	}
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_id', 'parent_id'], 'integer'],
            [['name', 'service.name', 'parent.name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Menu::find()
            ->with('parent')
            ->with('service')
	        ->joinWith([
		        'parent' => function($query) {
			        return $query->from = [
				        'p_menu' => 'menu',
			        ];
		        }
	        ])
	        ->joinWith('service');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

	    $dataProvider->sort->attributes['parent.name'] = [
		    'asc' => ['parent.name' => SORT_ASC],
		    'desc' => ['parent.name' => SORT_DESC],
	    ];

	    $dataProvider->sort->attributes['service.name'] = [
		    'asc' => ['service.name' => SORT_ASC],
		    'desc' => ['service.name' => SORT_DESC],
	    ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'service_id' => $this->service_id,
            'parent_id' => $this->parent_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
	        ->andFilterWhere(['menu.service_id' => $this->getAttribute('service.name')]);
	    if($this->getAttribute('parent.name') === 'null') {
		    $query->andFilterWhere(['menu.parent_id' => null]);
	    } else {
		    $query->andFilterWhere(['menu.parent_id' => $this->getAttribute('parent.name')]);
	    }

        return $dataProvider;
    }
}
