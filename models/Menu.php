<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $name
 * @property string $icon
 * @property string $route
 * @property integer $number
 * @property string $permission
 * @property integer $service_id
 * @property integer $parent_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Menu $parent
 * @property Menu[] $menus
 * @property Service $service
 */
class Menu extends ActiveRecord {
	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'route', 'number', 'service_id'], 'required'],
            [['service_id', 'parent_id'], 'integer'],
	        ['number', 'integer', 'min' => 1],
	        ['service_id', 'exist', 'targetClass' => Service::className(), 'targetAttribute' => 'id'],
	        [
		        'parent_id',
		        'exist',
		        'targetClass' => Menu::className(),
		        'targetAttribute' => 'id',
		        'when' => function($model) {
			        /* @var $model Menu */
			        return $model->parent_id != 0;
		        }
	        ],
	        ['parent_id', 'filter', 'filter' => function($value) {
		        if($value == 0) {
			        return null;
		        }
		        return $value;
	        }],
            [['permission'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['icon', 'route'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'icon' => 'Класс пиктограмы',
            'number' => '#',
	        'route' => 'Ссылка',
            'permission' => 'Разрешение',
            'service_id' => 'ID сервиса',
            'parent_id' => 'Родитель',
	        'created_at' => 'Создано',
	        'updated_at' => 'Обновлено',

	        'parent.name' => 'Родитель',
	        'service.name' => 'Сервис',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Menu::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

	public static function getParents($service_id, $is_new_record) {
		$parents = [
			'(не задано)',
		];
		$models = Menu::find()
			->where([
				'service_id' => $service_id,
				'parent_id' => null
			]);
		if(!$is_new_record) {
			$models->andWhere(['!=', 'id', $service_id]);
		}
		$models = $models->all();
		foreach ($models as $val) {
			/* @var $val Service */
			$parents[ $val->id ] = $val->name;
		}
		return $parents;
	}
	public static function getNumbers($service_id, $parent_id, $is_new_record) {
		if($parent_id == 0) {
			$parent_id = null;
		}
		$max = Menu::find()
			->where([
				'service_id' => $service_id,
				'parent_id' => $parent_id,
			])
			->max('number');
		$ret = [];
		for($i = 1; $i <= $max + (int)$is_new_record; $i++) {
			$ret[ $i ] = $i;
		}
		return $ret;
	}
}
