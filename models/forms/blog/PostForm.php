<?php

namespace app\models\forms\blog;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $nick
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $text
 * @property integer $status
 * @property boolean $picture
 * @property boolean $pic_in_post
 * @property integer $site_id
 * @property integer $publish_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property array $tags
 */
class PostForm extends Model {
	const STATUS_DRAFT = 0;
	const STATUS_PUBLISHED = 1;
	const STATUS_WAITING = 2;

	public static $statuses = [
		self::STATUS_DRAFT => 'Черновик',
		self::STATUS_PUBLISHED => 'Опубликовано',
		self::STATUS_WAITING => 'Ожидает',
	];

	public $id;
	public $nick;
	public $title;
	public $keywords;
	public $description;
	public $text;
	public $status;
	public $picture;
	public $pic_in_post;
	public $site_id;
	public $publish_at;
	public $author_id;
	public $tags;
	public $image_links;

	public $image;
	public $image_is_uploaded = false;

	public $isNewRecord = true;

	private $_marks;

    /**
     * @inheritdoc
     */
    public function rules() {
	    //@todo: сделать проверки на существование
        return [
            [['title', 'text'], 'required'],
			['marks', 'required', 'when' => function($model) {
				return (bool)$model->status;
			}, 'whenClient' => 'function (attribute, value) {
		        return $("#post-status").val() == "' . PostForm::STATUS_PUBLISHED . '";
		    }'],
			['title', 'trim'],
			['text', 'string'],
			[['status', 'picture', 'pic_in_post'], 'boolean'],
			[['nick', 'title'], 'string', 'max' => 100],
			['keywords', 'string', 'max' => 128],
			['description', 'string', 'max' => 250],
	        [
		        'image',
		        'file',
		        'skipOnEmpty' => false,
		        'when' => function($model) {
			        /* @var $model PostForm */
			        return (bool)$model->picture && !$model->image_is_uploaded;
		        },
		        'whenClient' => 'function (attribute, value) {
			        return $("#post-picture").prop("checked") && ' . (!$this->image_is_uploaded ? 'true' : 'false') . ';
			    }',
		        'uploadRequired' => 'Необходимо прикрепить картинку.',
	        ],
	        [
		        'image',
		        'file',
		        'extensions' => 'jpg, jpeg, gif',
		        'mimeTypes' => 'image/jpeg, image/gif',
	        ],
	        [['tags', 'publish_at'], 'safe'],
        ];
    }

	public function getMarks() {
		if($this->_marks) {
			return $this->_marks;
		}
		if(!$this->isNewRecord) {
			if(!is_array($this->tags)) {
				$this->tags = [];
			}
			return $this->_marks = implode(', ', $this->tags);
		}
		return null;
	}

	public function setMarks($val) {
		$this->_marks = $val;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nick' => 'Мнемоника',
            'title' => 'Название',
            'keywords' => 'Ключевые слова',
            'description' => 'Описание',
            'text' => 'Текст',
            'status' => 'Статус',
            'picture' => 'Картинка',
            'pic_in_post' => 'Показывать картинку внутри статьи?',
            'publish_at' => 'Дата публикации',
            'author_id' => 'Автор',
	        'marks' => 'Метки',
	        'image' => 'Картинка',
        ];
    }
}
