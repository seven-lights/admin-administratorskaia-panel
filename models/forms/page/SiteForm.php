<?php

namespace app\models\forms\page;

use Yii;

/**
 * This is the model class for table "site".
 *
 * @property integer $id
 * @property string $name
 * @property string $domain
 */
class SiteForm extends \app\models\forms\SiteForm {

}
