<?php

namespace app\models\forms\page;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "template".
 *
 * @property integer $id
 * @property string $name
 * @property string $head
 * @property string $body_start
 * @property string $body_end
 * @property integer $site_id
 */
class TemplateForm extends Model {

	public $id;
	public $name;
	public $head;
	public $body_start;
	public $body_end;
	public $site_id;

	public $isNewRecord = true;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	        [['name', 'site_id'], 'required'],
	        [['head', 'body_start', 'body_end'], 'string'],
	        ['site_id', 'integer'],
	        [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
	        'id' => 'ID',
	        'name' => 'Название',
	        'head' => 'В элемент head',
	        'body_start' => 'В начало элемента body',
	        'body_end' => 'В конец элемента body',
	        'site_id' => 'Site ID',
        ];
    }
}
