<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "site".
 *
 * @property integer $id
 * @property string $name
 * @property string $domain
 */
class SiteForm extends Model {
	public $id;
	public $name;
	public $domain;

	public $isNewRecord = true;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	        [['name', 'domain'], 'required'],
	        [['name', 'domain'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
	        'id' => 'ID',
	        'name' => 'Название',
	        'domain' => 'Домен',
        ];
    }
}
