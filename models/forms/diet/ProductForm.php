<?php

namespace app\models\forms\diet;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $protein
 * @property double $fat
 * @property double $carbohydrate
 * @property double $calorie
 * @property double $dietary_fiber
 * @property double $cholesterol
 * @property double $water
 * @property double $ca
 * @property double $fe
 * @property double $na
 * @property double $k
 * @property double $mg
 * @property double $p
 * @property double $co
 * @property double $cr
 * @property double $cu
 * @property double $mn
 * @property double $s
 * @property double $si
 * @property double $v
 * @property double $zn
 * @property double $mo
 * @property double $f
 * @property double $i
 * @property double $se
 * @property double $b
 * @property double $cl
 * @property double $a
 * @property double $b_car
 * @property double $b1
 * @property double $b2
 * @property double $b4
 * @property double $b5
 * @property double $b6
 * @property double $b7
 * @property double $b9
 * @property double $b12
 * @property double $c
 * @property double $d
 * @property double $e
 * @property double $pp
 * @property double $k_phylloquinone
 * @property double $tryptophan
 * @property double $lysine
 * @property double $methionine
 * @property double $threonine
 * @property double $leucine
 * @property double $isoleucine
 * @property double $phenylalanine
 * @property double $valine
 * @property integer $owner_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $copy_from
 * @property integer $category_id
 */
class ProductForm extends Model {
	public $id;
	public $name;
	public $description;
	public $protein;
	public $fat;
	public $carbohydrate;
	public $calorie;
	public $dietary_fiber;
	public $cholesterol;
	public $water;
	public $ca;
	public $fe;
	public $na;
	public $k;
	public $mg;
	public $p;
	public $co;
	public $cr;
	public $cu;
	public $mn;
	public $s;
	public $si;
	public $v;
	public $zn;
	public $mo;
	public $f;
	public $i;
	public $se;
	public $b;
	public $cl;
	public $a;
	public $b_car;
	public $b1;
	public $b2;
	public $b4;
	public $b5;
	public $b6;
	public $b7;
	public $b9;
	public $b12;
	public $c;
	public $d;
	public $e;
	public $pp;
	public $k_phylloquinone;
	public $tryptophan;
	public $lysine;
	public $methionine;
	public $threonine;
	public $leucine;
	public $isoleucine;
	public $phenylalanine;
	public $valine;
	public $owner_id;
	public $created_at;
	public $updated_at;
	public $copy_from;
	public $category_id;
	public $image;

	public $isNewRecord;
	public $image_links;

    /**
     * @inheritdoc
     */
	public function rules()
	{
		return [
			['name', 'required'],
			['name', 'string', 'max' => 50],
			['description', 'string'],
			[['protein', 'fat', 'carbohydrate', 'calorie', 'dietary_fiber', 'cholesterol', 'water', 'ca', 'fe', 'na', 'k', 'mg', 'p', 'co', 'cr', 'cu', 'mn', 's', 'si', 'v', 'zn', 'mo', 'f', 'i', 'se', 'b', 'cl', 'a', 'b_car', 'b1', 'b2', 'b4', 'b5', 'b6', 'b7', 'b9', 'b12', 'c', 'd', 'e', 'pp', 'k_phylloquinone', 'tryptophan', 'lysine', 'methionine', 'threonine', 'leucine', 'isoleucine', 'phenylalanine', 'valine'], 'number'],
			[['owner_id', 'copy_from', 'category_id'], 'integer'],
			['image_links', 'safe'],
			[
				'image',
				'file',
				'skipOnEmpty' => false,
				'when' => function($model) {
					/* @var $model ProductForm */
					return $model->isNewRecord;
				},
				'whenClient' => 'function (attribute, value) {
			        return ' . ($this->isNewRecord ? 'true' : 'false') . ';
			    }',
				'uploadRequired' => 'Необходимо прикрепить картинку.',
			],
			[
				'image',
				'file',
				'extensions' => 'jpg, jpeg, gif',
				'mimeTypes' => 'image/jpeg, image/gif',
			],
		];
	}

    /**
     * @inheritdoc
     */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Название',
			'description' => 'Описание',
			'protein' => 'Белки',
			'fat' => 'Жиры',
			'carbohydrate' => 'Углеводы',
			'calorie' => 'Калорийность',
			'dietary_fiber' => 'Пищевые волокна',
			'cholesterol' => 'Холестерин',
			'water' => 'Процент воды',
			'ca' => 'Кальций. Ca, мг',
			'fe' => 'Железо. Fe, мг',
			'na' => 'Натрий. Na, мг',
			'k' => 'Калий. K, мг',
			'mg' => 'Магний. Mg, мг',
			'p' => 'P, мг',
			'co' => 'Co, мг',
			'cr' => 'Cr, мг',
			'cu' => 'Cu, мг',
			'mn' => 'Mn, мг',
			's' => 'S, мг',
			'si' => 'Si, мг',
			'v' => 'V, мг',
			'zn' => 'Zn, мг',
			'mo' => 'Mo, мг',
			'f' => 'F, мг',
			'i' => 'I, мг',
			'se' => 'Se, мг',
			'b' => 'B, мг',
			'cl' => 'Cl, мг',
			'a' => 'A, мг',
			'b_car' => 'B-car, мг',
			'b1' => 'B1, мг',
			'b2' => 'B2, мг',
			'b4' => 'B4, мг',
			'b5' => 'B5, мг',
			'b6' => 'B6, мг',
			'b7' => 'B7, мг',
			'b9' => 'B9, мг',
			'b12' => 'B12, мг',
			'c' => 'C, мг',
			'd' => 'D, мг',
			'e' => 'E, мг',
			'pp' => 'PP, мг',
			'k_phylloquinone' => 'K (филлохинон), мг',
			'tryptophan' => 'Триптофан, мг',
			'lysine' => 'Лизин, мг',
			'methionine' => 'Метионин, мг',
			'threonine' => 'Треонин, мг',
			'leucine' => 'Лейцин, мг',
			'isoleucine' => 'Изолейцин, мг',
			'phenylalanine' => 'Фенилаланин, мг',
			'valine' => 'Валин, мг',
			'owner_id' => 'Владелец',
			'copy_from' => 'Копия от',
			'category_id' => 'Категория',
			'image' => 'Картинка',

			'category.name' => 'Категория',
		];
	}
}
