<?php

namespace app\models\forms\diet;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property integer $number
 * @property integer $parent_id
 */
class CategoryOfArticleForm extends Model {
	public $id;
	public $name;
	public $number;
	public $parent_id;

	public $isNewRecord = true;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
	        ['id', 'integer'],
	        [['name', 'number'], 'required'],
	        ['parent_id', 'integer'],
	        ['number', 'integer'],
	        ['name', 'string', 'max' => 50],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'number' => 'Номер',
            'parent_id' => 'ID родителя',
        ];
    }
}
