<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property string $name
 * @property string $nick
 * @property integer $many_sites
 * @property integer $creator_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $controllers
 *
 * @property User $creator
 */
class Service extends ActiveRecord {
	const MANY_SITES_ON = 1;
	const MANY_SITES_OFF = 0;

	public function behaviors() {
		return [
			TimestampBehavior::className(),
		];
	}
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'nick'], 'required'],
	        ['many_sites', 'boolean'],
	        ['controllers', 'string'],
            [['name', 'nick'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'nick' => 'Ник',
	        'many_sites' => 'Многосайтовость',
	        'controllers' => 'Контроллеры',
	        'created_at' => 'Создано',
	        'updated_at' => 'Обновлено',

            'creator_id' => 'Создатель',
	        'creator.nick' => 'Создатель',
        ];
    }

	public function beforeSave($insert) {
		if(parent::beforeSave($insert)) {
			if ($this->isNewRecord) {
				$this->creator_id = Yii::$app->user->id;
			}
			return true;
		}
		return false;
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }
}
