<?php

namespace app\controllers;

use app\extensions\Controller;
use app\models\forms\ServiceChangeForm;
use general\ext\api\BaseApi;
use Yii;
use app\models\Service;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class ServiceController extends Controller {
    public function actionChange() {
	    $this->layout = 'main-login';
	    $model = new ServiceChangeForm();
	    if(Yii::$app->request->isPost) {
		    $model->load(Yii::$app->request->post());

		    if($model->validate()) {
			    if($model->site == 0) {
				    /* @var $service Service */
				    $service = Service::findOne(['nick' => $model->service]);
				    if($service && $service->many_sites) {
					    return $this->redirect([$model->service . '/site/list']);
				    }
			    }
			    return $this->redirect([ $model->service . '/site/index', 'site' => $model->site ]);
		    }
	    }

	    $data = Service::find()
	        ->select('name, nick, many_sites')
	        ->asArray()
	        ->all();

	    return $this->render('change', [
            'data' => $data,
	        'model' => $model,
        ]);
    }
	public function actionGetSites($service) {
		if(!Yii::$app->request->isAjax) {
			throw new \HttpRequestMethodException();
		}
		/* @var $model Service */
		if(!$model = Service::findOne(['nick' => $service])) {
			throw new BadRequestHttpException();
		}
		if(!$model->many_sites) {
			throw new InvalidParamException();
		}
		Yii::$app->response->format = Response::FORMAT_JSON;

		$class = BaseApi::getInplementationName($service);
		/* @var BaseApi $class */
		return $class::getSites() + [0 => 'Управление сайтами'];
	}
}
