<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\controllers\diet;


use app\extensions\api\BlogAdminControllerTrait;
use app\extensions\Controller;

class ArticleController extends Controller {
	use BlogAdminControllerTrait;
	public $service = 'diet';
	public $defaultAction = 'admin';
	public static $domain = 'diet-articles';
}