<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\controllers\diet;


use app\extensions\Controller;
use app\models\forms\diet\ProductForm;
use app\models\search\ProductSearch;
use general\controllers\api\Controller as ApiController;
use general\ext\api\diet\DietApi;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class ProductController extends Controller {
	public $service = 'diet';
	public function actionIndex($page = 0) {
		$searchModel = new ProductSearch();
		$dataProvider = $searchModel->search([
			'page' => $page,
			'ProductSearch' => \Yii::$app->request->get('ProductSearch', []),
			'sort' => \Yii::$app->request->get('sort', ''),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}
	public function actionUpdate($id) {
		$model = new ProductForm();
		$model->isNewRecord = false;

		$res = DietApi::productUpdate($id);
		if($res['result'] == 'error') {
			throw new NotFoundHttpException();
		}
		$model->setAttributes($res['product']);

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			$model->image = UploadedFile::getInstance($model, 'image');
			if($model->validate()) {
				$res = DietApi::productUpdate(
					$id,
					array_merge(
						$model->getAttributes(null, ['id', 'created_at', 'updated_at', 'isNewRecord', 'image_links']),
						['image' => ($model->image
							? curl_file_create(
								$model->image->tempName,
								$model->image->type,
								$model->image->baseName . '.' . $model->image->extension)
							: '')
						]
					)
				);
				if($res['result'] == 'success') {
					$this->redirect(['update', 'id' => $id]);
				} else {
					$errors = [
						ApiController::ERROR_UNKNOWN => 'info',
						ApiController::ERROR_DB => 'info',
						ApiController::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						ApiController::ERROR_NO_DATA => 'info',

						ApiController::ERROR_ILLEGAL_PRODUCT_NAME => 'name',
						ApiController::ERROR_ILLEGAL_PRODUCT_DESCRIPTION => 'description',
						ApiController::ERROR_ILLEGAL_PRODUCT_PROTEIN => 'protein',
						ApiController::ERROR_ILLEGAL_PRODUCT_FAT => 'fat',
						ApiController::ERROR_ILLEGAL_PRODUCT_CARBOHYDRATE => 'carbohydrate',
						ApiController::ERROR_ILLEGAL_PRODUCT_CALORIE => 'calorie',
						ApiController::ERROR_ILLEGAL_PRODUCT_DIETARY_FIBER => 'dietary_fiber',
						ApiController::ERROR_ILLEGAL_PRODUCT_CHOLESTEROL => 'cholesterol',
						ApiController::ERROR_ILLEGAL_PRODUCT_WATER => 'water',
						ApiController::ERROR_ILLEGAL_PRODUCT_CA => 'ca',
						ApiController::ERROR_ILLEGAL_PRODUCT_FE => 'fe',
						ApiController::ERROR_ILLEGAL_PRODUCT_NA => 'na',
						ApiController::ERROR_ILLEGAL_PRODUCT_K => 'k',
						ApiController::ERROR_ILLEGAL_PRODUCT_MG => 'mg',
						ApiController::ERROR_ILLEGAL_PRODUCT_P => 'p',
						ApiController::ERROR_ILLEGAL_PRODUCT_CO => 'co',
						ApiController::ERROR_ILLEGAL_PRODUCT_CR => 'cr',
						ApiController::ERROR_ILLEGAL_PRODUCT_CU => 'cu',
						ApiController::ERROR_ILLEGAL_PRODUCT_MN => 'mn',
						ApiController::ERROR_ILLEGAL_PRODUCT_S => 's',
						ApiController::ERROR_ILLEGAL_PRODUCT_SI => 'si',
						ApiController::ERROR_ILLEGAL_PRODUCT_V => 'v',
						ApiController::ERROR_ILLEGAL_PRODUCT_ZN => 'zn',
						ApiController::ERROR_ILLEGAL_PRODUCT_MO => 'mo',
						ApiController::ERROR_ILLEGAL_PRODUCT_F => 'f',
						ApiController::ERROR_ILLEGAL_PRODUCT_I => 'i',
						ApiController::ERROR_ILLEGAL_PRODUCT_SE => 'se',
						ApiController::ERROR_ILLEGAL_PRODUCT_B => 'b',
						ApiController::ERROR_ILLEGAL_PRODUCT_CL => 'cl',
						ApiController::ERROR_ILLEGAL_PRODUCT_A => 'a',
						ApiController::ERROR_ILLEGAL_PRODUCT_B_CAR => 'b_car',
						ApiController::ERROR_ILLEGAL_PRODUCT_B1 => 'b1',
						ApiController::ERROR_ILLEGAL_PRODUCT_B2 => 'b2',
						ApiController::ERROR_ILLEGAL_PRODUCT_B4 => 'b4',
						ApiController::ERROR_ILLEGAL_PRODUCT_B5 => 'b5',
						ApiController::ERROR_ILLEGAL_PRODUCT_B6 => 'b6',
						ApiController::ERROR_ILLEGAL_PRODUCT_B7 => 'b7',
						ApiController::ERROR_ILLEGAL_PRODUCT_B9 => 'b9',
						ApiController::ERROR_ILLEGAL_PRODUCT_B12 => 'b12',
						ApiController::ERROR_ILLEGAL_PRODUCT_C => 'c',
						ApiController::ERROR_ILLEGAL_PRODUCT_D => 'd',
						ApiController::ERROR_ILLEGAL_PRODUCT_E => 'e',
						ApiController::ERROR_ILLEGAL_PRODUCT_PP => 'pp',
						ApiController::ERROR_ILLEGAL_PRODUCT_K_PHYLLOQUINONE => 'k_phylloquinone',
						ApiController::ERROR_ILLEGAL_PRODUCT_TRYPTOPHAN => 'tryptophan',
						ApiController::ERROR_ILLEGAL_PRODUCT_LYSINE => 'lysine',
						ApiController::ERROR_ILLEGAL_PRODUCT_METHIONINE => 'methionine',
						ApiController::ERROR_ILLEGAL_PRODUCT_THREONINE => 'threonine',
						ApiController::ERROR_ILLEGAL_PRODUCT_LEUCINE => 'leucine',
						ApiController::ERROR_ILLEGAL_PRODUCT_ISOLEUCINE => 'isoleucine',
						ApiController::ERROR_ILLEGAL_PRODUCT_PHENYLALANINE => 'phenylalanine',
						ApiController::ERROR_ILLEGAL_PRODUCT_VALINE => 'valine',
						ApiController::ERROR_ILLEGAL_PRODUCT_OWNER_ID => 'owner_id',
						ApiController::ERROR_ILLEGAL_PRODUCT_COPY_FROM => 'copy_from',
						ApiController::ERROR_ILLEGAL_PRODUCT_CATEGORY_ID => 'category_id',
						ApiController::ERROR_ILLEGAL_PRODUCT_IMAGE => 'image',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == ApiController::ERROR_NO_SITE
							|| $error['code'] == ApiController::ERROR_NO_POST) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', ApiController::$_errors[ ApiController::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}
	public function actionCreate() {
		$model = new ProductForm();

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			$model->image = UploadedFile::getInstance($model, 'image');
			if($model->validate()) {
				$res = DietApi::productCreate(
					array_merge(
						$model->getAttributes(null, ['id', 'created_at', 'updated_at', 'isNewRecord', 'image_links']),
						['image' => ($model->image
							? curl_file_create(
								$model->image->tempName,
								$model->image->type,
								$model->image->baseName . '.' . $model->image->extension)
							: '')
						]
					)
				);
				if($res['result'] == 'success') {
					$this->redirect(['index']);
				} else {
					$errors = [
						ApiController::ERROR_UNKNOWN => 'info',
						ApiController::ERROR_DB => 'info',
						ApiController::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						ApiController::ERROR_NO_DATA => 'info',

						ApiController::ERROR_ILLEGAL_PRODUCT_NAME => 'name',
						ApiController::ERROR_ILLEGAL_PRODUCT_DESCRIPTION => 'description',
						ApiController::ERROR_ILLEGAL_PRODUCT_PROTEIN => 'protein',
						ApiController::ERROR_ILLEGAL_PRODUCT_FAT => 'fat',
						ApiController::ERROR_ILLEGAL_PRODUCT_CARBOHYDRATE => 'carbohydrate',
						ApiController::ERROR_ILLEGAL_PRODUCT_CALORIE => 'calorie',
						ApiController::ERROR_ILLEGAL_PRODUCT_DIETARY_FIBER => 'dietary_fiber',
						ApiController::ERROR_ILLEGAL_PRODUCT_CHOLESTEROL => 'cholesterol',
						ApiController::ERROR_ILLEGAL_PRODUCT_WATER => 'water',
						ApiController::ERROR_ILLEGAL_PRODUCT_CA => 'ca',
						ApiController::ERROR_ILLEGAL_PRODUCT_FE => 'fe',
						ApiController::ERROR_ILLEGAL_PRODUCT_NA => 'na',
						ApiController::ERROR_ILLEGAL_PRODUCT_K => 'k',
						ApiController::ERROR_ILLEGAL_PRODUCT_MG => 'mg',
						ApiController::ERROR_ILLEGAL_PRODUCT_P => 'p',
						ApiController::ERROR_ILLEGAL_PRODUCT_CO => 'co',
						ApiController::ERROR_ILLEGAL_PRODUCT_CR => 'cr',
						ApiController::ERROR_ILLEGAL_PRODUCT_CU => 'cu',
						ApiController::ERROR_ILLEGAL_PRODUCT_MN => 'mn',
						ApiController::ERROR_ILLEGAL_PRODUCT_S => 's',
						ApiController::ERROR_ILLEGAL_PRODUCT_SI => 'si',
						ApiController::ERROR_ILLEGAL_PRODUCT_V => 'v',
						ApiController::ERROR_ILLEGAL_PRODUCT_ZN => 'zn',
						ApiController::ERROR_ILLEGAL_PRODUCT_MO => 'mo',
						ApiController::ERROR_ILLEGAL_PRODUCT_F => 'f',
						ApiController::ERROR_ILLEGAL_PRODUCT_I => 'i',
						ApiController::ERROR_ILLEGAL_PRODUCT_SE => 'se',
						ApiController::ERROR_ILLEGAL_PRODUCT_B => 'b',
						ApiController::ERROR_ILLEGAL_PRODUCT_CL => 'cl',
						ApiController::ERROR_ILLEGAL_PRODUCT_A => 'a',
						ApiController::ERROR_ILLEGAL_PRODUCT_B_CAR => 'b_car',
						ApiController::ERROR_ILLEGAL_PRODUCT_B1 => 'b1',
						ApiController::ERROR_ILLEGAL_PRODUCT_B2 => 'b2',
						ApiController::ERROR_ILLEGAL_PRODUCT_B4 => 'b4',
						ApiController::ERROR_ILLEGAL_PRODUCT_B5 => 'b5',
						ApiController::ERROR_ILLEGAL_PRODUCT_B6 => 'b6',
						ApiController::ERROR_ILLEGAL_PRODUCT_B7 => 'b7',
						ApiController::ERROR_ILLEGAL_PRODUCT_B9 => 'b9',
						ApiController::ERROR_ILLEGAL_PRODUCT_B12 => 'b12',
						ApiController::ERROR_ILLEGAL_PRODUCT_C => 'c',
						ApiController::ERROR_ILLEGAL_PRODUCT_D => 'd',
						ApiController::ERROR_ILLEGAL_PRODUCT_E => 'e',
						ApiController::ERROR_ILLEGAL_PRODUCT_PP => 'pp',
						ApiController::ERROR_ILLEGAL_PRODUCT_K_PHYLLOQUINONE => 'k_phylloquinone',
						ApiController::ERROR_ILLEGAL_PRODUCT_TRYPTOPHAN => 'tryptophan',
						ApiController::ERROR_ILLEGAL_PRODUCT_LYSINE => 'lysine',
						ApiController::ERROR_ILLEGAL_PRODUCT_METHIONINE => 'methionine',
						ApiController::ERROR_ILLEGAL_PRODUCT_THREONINE => 'threonine',
						ApiController::ERROR_ILLEGAL_PRODUCT_LEUCINE => 'leucine',
						ApiController::ERROR_ILLEGAL_PRODUCT_ISOLEUCINE => 'isoleucine',
						ApiController::ERROR_ILLEGAL_PRODUCT_PHENYLALANINE => 'phenylalanine',
						ApiController::ERROR_ILLEGAL_PRODUCT_VALINE => 'valine',
						ApiController::ERROR_ILLEGAL_PRODUCT_OWNER_ID => 'owner_id',
						ApiController::ERROR_ILLEGAL_PRODUCT_COPY_FROM => 'copy_from',
						ApiController::ERROR_ILLEGAL_PRODUCT_CATEGORY_ID => 'category_id',
						ApiController::ERROR_ILLEGAL_PRODUCT_IMAGE => 'image',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == ApiController::ERROR_NO_SITE) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', ApiController::$_errors[ ApiController::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}
	public function actionDelete($id) {
		DietApi::productDelete($id);
		$this->redirect(['index']);
	}
}