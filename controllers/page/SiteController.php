<?php

namespace app\controllers\page;

use app\extensions\Controller;
use app\models\forms\page\SiteForm;
use app\models\search\SiteSearch;
use general\controllers\api\Controller as ApiController;
use general\ext\api\page\PageApi;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class SiteController extends Controller {
	public $service = 'page';
    public function actionIndex()
    {
        return $this->render('index');
    }
	public function actionCreate() {
		$model = new SiteForm();

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			if($model->validate()) {
				$res = PageApi::siteCreate($model->getAttributes());
				if($res['result'] == 'success') {
					$this->redirect(['list']);
				} else {
					$errors = [
						ApiController::ERROR_UNKNOWN => 'info',
						ApiController::ERROR_DB => 'info',
						ApiController::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						ApiController::ERROR_NO_DATA => 'info',

						ApiController::ERROR_ILLEGAL_SITE_NAME => 'name',
						ApiController::ERROR_ILLEGAL_SITE_DOMAIN => 'domain',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == ApiController::ERROR_NO_SITE) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', ApiController::$_errors[ ApiController::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('create', [
			'model' => $model,
		]);
	}
	public function actionUpdate() {
		if(!self::$site) {
			throw new BadRequestHttpException();
		}
		$site = PageApi::siteView(self::$site);
		if($site['result'] == 'error') {
			throw new NotFoundHttpException();
		}

		$model = new SiteForm();
		$model->isNewRecord = false;

		$model->setAttributes($site['site']);

		if(\Yii::$app->request->isPost) {
			$model->load(\Yii::$app->request->post());
			if($model->validate()) {
				$res = PageApi::siteUpdate($site['site']['domain'], $model->getAttributes());
				if($res['result'] == 'success') {
					$this->redirect(['list']);
				} else {
					$errors = [
						ApiController::ERROR_UNKNOWN => 'info',
						ApiController::ERROR_DB => 'info',
						ApiController::ERROR_ILLEGAL_REQUEST_METHOD => 'info',
						ApiController::ERROR_NO_DATA => 'info',

						ApiController::ERROR_ILLEGAL_SITE_NAME => 'name',
						ApiController::ERROR_ILLEGAL_SITE_DOMAIN => 'domain',
					];
					foreach ($res['errors'] as $error) {
						if($error['code'] == ApiController::ERROR_NO_SITE
							|| $error['code'] == ApiController::ERROR_NO_POST) {
							throw new NotFoundHttpException();
						}
						if(isset($errors[ $error['code'] ])) {
							$model->addError($errors[ $error['code'] ], $error['title']);
						} else {
							$model->addError('unknown', ApiController::$_errors[ ApiController::ERROR_UNKNOWN ]);
						}
					}
				}
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}
	public function actionDelete() {
		if(!self::$site) {
			throw new BadRequestHttpException();
		}
		$site = PageApi::siteView(self::$site);
		if($site['result'] == 'error') {
			throw new NotFoundHttpException();
		}
		PageApi::siteDelete($site['site']['domain']);
		$this->redirect(['list']);
	}
	public function actionList() {
		$searchModel = new SiteSearch();
		$dataProvider = $searchModel->search();

		return $this->render('list', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}
}
