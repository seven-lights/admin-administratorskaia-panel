<?php
/**
 * Project: Diet (My balanced diet) - Seven Lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\controllers\page;


use app\extensions\api\page\TemplateAdminControllerTrait;
use app\extensions\Controller;
use general\ext\api\page\PageApi;
use yii\web\BadRequestHttpException;

class TemplateController extends Controller {
	use TemplateAdminControllerTrait;
	public $service = 'page';
	public static $domain;
	public function __construct($id, $module, $config = []) {
		parent::__construct($id, $module, $config);
		if(!self::$site) {
			throw new BadRequestHttpException();
		}
		$site = PageApi::siteView(self::$site);
		if($site['result'] == 'error') {
			throw new BadRequestHttpException();
		}
		self::$domain = $site['site']['domain'];
	}
}